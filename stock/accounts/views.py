from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login,logout,update_session_auth_hash
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
# Create your views here.
def signIn(request):
    if request.method=='POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/dashboard/')
        else:
            pass
    return render(request,'accounts/signIn.html',context={})

def forgotPassword(request):
    return render(request,'accounts/forgotPassword.html',context={})

@login_required(login_url='/accounts/signIn/')
def changePassword(request):
    if request.method=='POST':
        oldpassword=request.POST['oldpassword']
        confirmpassword=request.POST['confirmpassword']
        newpassword=request.POST['newpassword']
        user=User.objects.get(username=request.user.username)
        match_check=user.check_password(oldpassword)
        if match_check is True:
            # Updating a user's password logs out all sessions for the user if django.contrib.auth.middleware.SessionAuthenticationMiddleware is enabled.
            user.set_password(newpassword)
            user.save()
            messages.success(request,'password changes successfully')
        else:
            messages.error(request,'your old password is not coorect')
    return render(request,'accounts/changePassword.html',context={})

def logout_view(request):
    logout(request)
    return redirect('/accounts/signIn/')

