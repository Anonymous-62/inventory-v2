from django.shortcuts import render,redirect
from django.http import JsonResponse
from.models import Product,Supplier,Supplied,PurchaseReturned,Customer,Sale,SaleReturn
from django.template.loader import render_to_string
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.contrib.auth.decorators import login_required
# Operation Perform on Product

@login_required(login_url='/accounts/signIn/')
def dashboard(request):
    return render(request,'dashboard/dashboard.html',context={})

@login_required(login_url='/accounts/signIn/')
def createProduct(request):
    if request.method=='POST':
        product=request.POST['product']
        price=request.POST['price']
        save_Product(product,price)
    product__obj=Product.objects.all()
    paginator=Paginator(product__obj,12)
    page_number=request.GET.get('page')
    try:
        product__obj=paginator.page(page_number)
    except PageNotAnInteger:
        product__obj=paginator.page(1)
    except EmptyPage:
        product__obj=paginator.page(paginator.num_pages)
    return render(request,'dashboard/product.html',context={'items':product__obj})

@login_required(login_url='/accounts/signIn/')
def updateProduct(request):
    data=dict()
    if request.method=='POST':
        try:
            product=request.POST['product']
            price=request.POST['price']
            pk=request.POST['pk']
            update__product(product,price,pk)
        except Exception as error:
            print(error)
        return redirect('/dashboard/product/')

    else:
        pk=request.GET.get('id')
        items=Product.objects.get(pk=pk)
        context={'title':items.title,'price':items.price,'pk':items.pk
        }
        data['modal_for_updateProduct']=render_to_string('dashboard/modal_for_updateProduct.html',request=request)
        data['data']=context
        return JsonResponse(data)
        
@login_required(login_url='/accounts/signIn/')
def delete_product(request):
    pk=request.GET.get('pk')
    product__obj=Product.objects.get(pk=pk)
    product__obj.delete()
    return redirect('/dashboard/product/')
  
@login_required(login_url='/accounts/signIn/')
def save_Product(product,price):
    try:
        product__obj=Product.objects.get(title=product)
        if product__obj:
            pass
    except Exception as error:
        print(error)
        product__obj=Product(title=product,price=price)
        product__obj.save()
        return product__obj

@login_required(login_url='/accounts/signIn/')
def update__product(product,price,pk):
    try:
        product__obj=Product.objects.get(pk=pk)
        if product__obj:
            product__obj.price=price
            product__obj.title=product
            product__obj.save()
            print('product update')
    except Exception as error:
        print(error)

@login_required(login_url='/accounts/signIn/')
def createSupplier(request):
    try:
        if request.method=='POST':
            supplier=request.POST['supplier']
            address=request.POST['address']
            contact=request.POST['contact']
            supplier__obj=Supplier(name=supplier,contact=contact,address=address)
            supplier__obj.save()
    except Exception as error:
        print(error)
    supplier__obj=Supplier.objects.all()
    paginator=Paginator(supplier__obj,12)
    page_number=request.GET.get('page')
    try:
        supplier__obj=paginator.page(page_number)
    except PageNotAnInteger:
        supplier__obj=paginator.page(1)
    except EmptyPage:
        supplier__obj=paginator.page(paginator.num_pages)
   
    return render(request,'dashboard/suppliers.html',context={'items':supplier__obj})


@login_required(login_url='/accounts/signIn/')
def updateSupplier(request):
    try:
        data=dict()
        if request.method=='POST':
            supplier=request.POST['supplier']
            address=request.POST['address']
            contact=request.POST['contact']
            pk=request.POST['pk']
            print(pk)
            supplier__obj=Supplier.objects.get(pk=pk)
            supplier__obj.name=supplier
            supplier__obj.contact=contact
            supplier__obj.address=address
            supplier__obj.save()
            return redirect('/dashboard/supplier/')
    except Exception as error:
        print(error)
    pk=request.GET.get('pk')
    supplier__obj=Supplier.objects.get(pk=pk)
    context={'supplier':supplier__obj.name,'contact':supplier__obj.contact,'address':supplier__obj.address,'pk':supplier__obj.pk}
    data['modal_for_updateSupplier']=render_to_string('dashboard/modal_for_updateSupplier.html',context,request=request)
    return JsonResponse(data)

@login_required(login_url='/accounts/signIn/')
def purchaseProduct(request):
    if request.method=='POST':
        supplier=request.POST['selected_supplier']
        product=request.POST['selected_product']
        quantity=request.POST['quantity']
        product__obj=Product.objects.get(title=product)
        supplier__obj=Supplier.objects.get(name=supplier)
        supplied__obj=Supplied(product=product__obj,supplier=supplier__obj,quantity=quantity)
        supplied__obj.save()
    data=dict()
    supplied__obj=Supplied.objects.all()
    supplier__obj=Supplier.objects.all()
    product__obj=Product.objects.all()
    paginator=Paginator(supplied__obj,8)
    page_number=request.GET.get('page')
    try:
        supplied__obj=paginator.page(page_number)
    except PageNotAnInteger:
        supplied__obj=paginator.page(1)
    except EmptyPage:
        supplied__obj=paginator.page(paginator.num_pages)
   
    return render(request,'dashboard/purchase.html',context={'items':supplied__obj,'products':product__obj,'suppliers':supplier__obj})


@login_required(login_url='/accounts/signIn/')
def updatePurchaseProduct(request):
    data=dict()
    if request.method=='POST':
        print('post call')
        supplier=request.POST['selected_supplier']
        product=request.POST['selected_product']
        quantity=request.POST['quantity']
        product__obj=Product.objects.get(title=product)
        supplier__obj=Supplier.objects.get(name=supplier)
        pk=request.POST['pk']
        print(pk)
        supplied__obj=Supplied.objects.get(pk=pk)
        supplied__obj.product=product__obj
        supplied__obj.supplier=supplier__obj
        supplied__obj.quantity=quantity
        supplied__obj.save()
        return redirect('/dashboard/purchase/product/')
    pk=request.GET.get('pk')
    supplied__obj=Supplied.objects.get(pk=pk)
    supplier__obj=Supplier.objects.all()
    product__obj=Product.objects.all()
    context={
            'supplied':supplied__obj,'products':product__obj,'suppliers':supplier__obj }
    data['modal_for_updatePurchaseProduct']=render_to_string('dashboard/modal_for_updatePurchaseProduct.html',context,request=request)
    return JsonResponse(data)



@login_required(login_url='/accounts/signIn/')
def purchaseReturn(request):
    if request.method=='POST':
        supplier=request.POST['selected_supplier']
        product=request.POST['selected_product']
        quantity=request.POST['quantity']
        product__obj=Product.objects.get(title=product)
        supplier__obj=Supplier.objects.get(name=supplier)
        purchaseReturn__obj=PurchaseReturned(product=product__obj,supplier=supplier__obj,quantity=quantity)
        purchaseReturn__obj.save()
    data=dict()
    purchaseReturn__obj=PurchaseReturned.objects.all()
    supplier__obj=Supplier.objects.all()
    product__obj=Product.objects.all()
    paginator=Paginator(purchaseReturn__obj,8)
    page_number=request.GET.get('page')
    try:
        purchaseReturn__obj=paginator.page(page_number)
    except PageNotAnInteger:
        purchaseReturn__obj=paginator.page(1)
    except EmptyPage:
        purchaseReturn__obj=paginator.page(paginator.num_pages)
    return render(request,'dashboard/return_product.html',context={'items':purchaseReturn__obj,'products':product__obj,'suppliers':supplier__obj})


@login_required(login_url='/accounts/signIn/')
def update_retured_product(request):
    data=dict()
    if request.method=='POST':
        try:
            supplier=request.POST['selected_supplier']
            product=request.POST['selected_product']
            quantity=request.POST['quantity']
            product__obj=Product.objects.get(title=product)
            supplier__obj=Supplier.objects.get(name=supplier)
            pk=request.POST['pk']
            purchaseReturn__obj=PurchaseReturned.objects.get(pk=pk)
            purchaseReturn__obj.supplier=supplier__obj
            purchaseReturn__obj.product=product__obj
            purchaseReturn__obj.quantity=quantity
            purchaseReturn__obj.save()
            return redirect('/dashboard/purchase/return/')

        except Exception as error:
            print(error)

    else:
        pk=request.GET.get('id')
        purchaseReturn__obj=PurchaseReturned.objects.get(pk=pk)
        product__obj=Product.objects.all()
        supplier__obj=Supplier.objects.all()
        context={'items':purchaseReturn__obj,'products':product__obj,'suppliers':supplier__obj
        }
        data['modal_for_UpdateReturnProduct']=render_to_string('dashboard/modal_for_UpdateReturnProduct.html',context,request=request)
        return JsonResponse(data)



@login_required(login_url='/accounts/signIn/')
def createCustomer(request):
    if request.method=='POST':
        try:
            name=request.POST['name']
            email=request.POST['email']
            contact=request.POST['contact']
            address=request.POST['address']
            customer__obj=Customer(name=name,email=email,contact=contact,address=address)
            customer__obj.save()
        except Exception as error:
            print(error)
    customer__obj=Customer.objects.all()
    paginator=Paginator(customer__obj,8)
    page_number=request.GET.get('page')
    try:
        customer__obj=paginator.page(page_number)
    except PageNotAnInteger:
        customer__obj=paginator.page(1)
    except EmptyPage:
        customer__obj=paginator.page(paginator.num_pages)
    return render(request,'dashboard/customer.html',context={'customers':customer__obj})


@login_required(login_url='/accounts/signIn/')
def cutomer_update(request):
    if request.method=='POST':
        pk=request.POST['pk']
        name=request.POST['name']
        email=request.POST['email']
        contact=request.POST['contact']
        address=request.POST['address']
        customer__obj=Customer.objects.get(pk=pk)
        customer__obj.name=name
        customer__obj.email=email
        customer__obj.contact=contact
        customer__obj.address=address
        customer__obj.save()
        customer__obj=Customer.objects.all()
        return render(request,'dashboard/customer.html',context={'customers':customer__obj})
    
    data=dict()
    pk=request.GET.get('pk')
    customer__obj=Customer.objects.get(pk=pk)
    context={'name':customer__obj.name,'email':customer__obj.email,'contact':customer__obj.contact,'address':customer__obj.address,'pk':customer__obj.pk}
    data['modal_for_updateCustomer']=render_to_string('dashboard/modal_for_updateCustomer.html',context,request=request)
    return JsonResponse(data)
   
  

def get_sum_of_quantity(request):
    quantity_sum=''
    try:
        name=request.GET.get('product')
        product__obj=Product.objects.get(title=name)
        temp=product__obj.supplied.all()
        quantity_sum=sum([read.quantity for read in temp])
        sale__obj=product__obj.sale.all()
        if sale__obj:
            sale_quantity=sum([sale.quantity for sale in sale__obj])
            quantity_sum=quantity_sum-sale_quantity
        
    except Exception as error:
        print(error)
    return JsonResponse({'quantity':quantity_sum})

@login_required(login_url='/accounts/signIn/')
def sale(request):
    if request.method=='POST':
        product=request.POST['selected_product']
        quantity=request.POST['quantity']
        customer=request.POST['selected_customer']
        product__obj=Product.objects.get(title=product)
        customer__obj=Customer.objects.get(name=customer)
        sale__obj=Sale(product=product__obj,customer=customer__obj,quantity=quantity)
        sale__obj.save()
    list=[]
    remove_duplicate=[]
    product__obj=Supplied.objects.all()
    for i in product__obj:
        list.append(i.product.title)
    for read in list:
        if read not in remove_duplicate:
            remove_duplicate.append(read)
    customer__obj=Customer.objects.all()
    sale__obj=Sale.objects.all()
    paginator=Paginator(sale__obj,8)
    page_number=request.GET.get('page')
    try:
        sale__obj=paginator.page(page_number)
    except PageNotAnInteger:
        sale__obj=paginator.page(1)
    except EmptyPage:
        sale__obj=paginator.page(paginator.num_pages)
   
    return render(request,'dashboard/sale.html',context={'sales':sale__obj,'products':remove_duplicate,'customers':customer__obj})

def deleteSale(request,pk):
    print('delete')
    Sale.objects.get(pk=pk).delete()
    return redirect('/dashboard/sale/')

@login_required(login_url='/accounts/signIn/')
def sale_update(request):
    data=dict()
    pk=''
    sale__obj=''
    try:   
        if request.method=='POST':
            product=request.POST['selected_product']
            quantity=request.POST['quantity']
            customer=request.POST['selected_customer']
            pk=request.POST['pk']
            print(pk)
            product__obj=Product.objects.get(title=product)
            customer__obj=Customer.objects.get(name=customer)   
            print(Sale.objects.all())
            sale__obj=Sale.objects.get(pk=pk
            )
            sale__obj.product=product__obj
            sale__obj.quantity=quantity
            sale__obj.customer=customer__obj
            sale__obj.save()
            return redirect('/dashboard/sale/')
        else:
            pk=request.GET.get('pk')
            sale__obj=Sale.objects.get(pk=pk)
    
        product__obj=Product.objects.all()
        customer__obj=Customer.objects.all()
        data['modal_for_updateSale']=render_to_string('dashboard/modal_for_updateSale.html',context={'sale':sale__obj,'products':product__obj,'customers':customer__obj},request=request)
        return JsonResponse(data)

    except Exception as error:
        print(error)
    
@login_required(login_url='/accounts/signIn/')
def sales_Return(request):
    if request.method=='POST':
        product=request.POST['selected_product']
        quantity=request.POST['quantity']
        customer=request.POST['selected_customer']
        product__obj=Product.objects.get(title=product)
        customer__obj=Customer.objects.get(name=customer)
        salesReturn__obj=SaleReturn(product=product__obj,customer=customer__obj,quantity=quantity)
        salesReturn__obj.save()
    salesReturn__obj=SaleReturn.objects.all()
    product__obj=Product.objects.all()
    customer__obj=Customer.objects.all()
    paginator=Paginator(salesReturn__obj,8)
    page_number=request.GET.get('page')
    try:
        salesReturn__obj=paginator.page(page_number)
    except PageNotAnInteger:
        salesReturn__obj=paginator.page(1)
    except EmptyPage:
        salesReturn__obj=paginator.page(paginator.num_pages)
   
    return render(request,'dashboard/saleReturn.html',context={'salesReturn':salesReturn__obj,'products':product__obj,'customers':customer__obj})

@login_required(login_url='/accounts/signIn/')
def updateSalesReturn(request):
    data=dict()
    pk=''
    sale__obj=''
    try:   
        if request.method=='POST':
            product=request.POST['selected_product']
            quantity=request.POST['quantity']
            customer=request.POST['selected_customer']
            pk=request.POST['pk']
            print(pk)
            product__obj=Product.objects.get(title=product)
            customer__obj=Customer.objects.get(name=customer)   
            salesReturn__obj=SaleReturn.objects.get(pk=pk)
            salesReturn__obj.product=product__obj
            salesReturn__obj.quantity=quantity
            salesReturn__obj.customer=customer__obj
            salesReturn__obj.save()
            return redirect('/dashboard/sale/return/')
        else:
            print('else execute')
            pk=request.GET.get('pk')
            salesReturn__obj=SaleReturn.objects.get(pk=pk)
        product__obj=Product.objects.all()
        customer__obj=Customer.objects.all()
        data['modal_for_updateSaleReturn']=render_to_string('dashboard/modal_for_updatesalesReturn.html',context={'salesReturn':salesReturn__obj,'products':product__obj,'customers':customer__obj},request=request)
        return JsonResponse(data)

    except Exception as error:
        print(error)

def stock(request):
    list=[]
    data=dict()
    list_for_sale=[]
    dict_for_sale=dict()
    quantity=int()
    product__obj=Product.objects.all()
    # sum of purchase quantity
    for pr_read in product__obj:
        for pur_read in pr_read.supplied.all():
            quantity=pur_read.quantity+quantity
        if quantity>0:
            data['name']=pr_read.title
            data['quantity']=quantity
            list.append(data)
            data=dict()
            quantity=int()
    # print(list)
    
    # sum of sale quantity
    for pr_read in product__obj:
        for sale_read in pr_read.sale.all():
            quantity=sale_read.quantity+quantity
        if quantity>0:
            dict_for_sale['name']=pr_read.title
            dict_for_sale['quantity']=quantity
            list_for_sale.append(dict_for_sale)
            dict_for_sale=dict()
            quantity=int()
    # print(list_for_sale)
    # subtract purchase quantity with sale quantity
    for sale_list in list_for_sale:
        for purchase_list in list:
            if sale_list['name']==purchase_list['name']:
                purchase_list['quantity']=purchase_list['quantity']-sale_list['quantity']

    # add sales quantity with salereturn
    return render(request,'dashboard/stock.html',context={'items':list})

# https://www.codegrepper.com/code-examples/whatever/date+picker+in+bootstrap+4
from datetime import *
def search_product_customer_date_wise_record(request):
    data=dict()
    sale__obj=''
    product=request.POST['product']
    customer=request.POST['customer']
    date=request.POST['date']
    if product and customer and date:
        product__obj=Product.objects.get(title=product)
        customer__obj=Customer.objects.get(name=customer)
        sale__obj=Sale.objects.filter(product=product__obj,customer=customer__obj,crated_at=date)
        if sale__obj:
            data['filter']=render_to_string('dashboard/filter.html',context={'sales':sale__obj},request=request)
            return JsonResponse(data)
    elif product and customer:
        sale__obj=Sale.objects.filter(product__title__iexact=product,customer__name__iexact=customer)
        return render(request,'dashboard/sale.html',context={'sales':sale__obj})

    elif product and date:
        data['filter']=render_to_string('dashboard/sale.html',context={'sales':sale__obj},request=request)
        return JsonResponse(data)
    elif customer and date:
        pass
    elif customer:
        sale__obj=Sale.objects.filter(customer__name__icontains=customer)
        return render(request,'dashboard/sale.html',context={'sales':sale__obj})
        print(sale__obj)

    elif product:
        sale__obj=Sale.objects.filter(product__title__icontains=product)
        return render(request,'dashboard/sale.html',context={'sales':sale__obj})
        print(sale__obj)

    else:
        date=datetime.strptime(date, '%Y/%m/%d').date()
        sale__obj=Sale.objects.filter(
            crated_at__day=date.day,
            crated_at__month=date.month,
            crated_at__year=date.year
        )
        print(sale__obj)
        return render(request,'dashboard/sale.html',context={'sales':sale__obj})


     
